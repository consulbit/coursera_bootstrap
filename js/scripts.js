$(document).ready(function () {
    $('#carousel').carousel();
    // Carousel onPause
    $('#carousel-button').click(function () {
        if ($(this).children("span").hasClass("fa-pause")) {
            $('#carousel').carousel('pause');
            $(this).children("span").removeClass('fa-pause');
            $(this).children("span").addClass('fa-play');
        } else if ($(this).children("span").hasClass('fa-play')) {
            $('#carousel').carousel('cycle');
            $(this).children("span").removeClass('fa-play');
            $(this).children("span").addClass('fa-pause');
        }
    });

    // Login Modal
    $('#loginModal').modal({
        show: false
    });
    $('#loginModalButton').on('click', function () {
        $('#loginModal').modal('show');
    });

    // Reserve Table Modal
    $('#reservationTableModal').modal({ show: false });
    $('#reserveTableButton').on('click', function () {
        $('#reservationTableModal').modal('show');
    })
});